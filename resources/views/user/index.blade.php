<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Ready | Test</title>
</head>

<body>
    <div class="container" style="margin-top:50px">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Usuarios Registrados
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped table-hover">
                            <thead class="table-dark">
                                <tr style="text-align:center;font-size: 12px;">
                                    <th scope="col">id</th>
                                    <th scope="col">Segmentation</th>
                                    <th scope="col">Program</th>
                                    <th scope="col">User</th>
                                    <th scope="col">Identification Type</th>
                                    <th scope="col">Identification Number</th>
                                    <th scope="col">Mobile Number</th>
                                    <th scope="col">Meta</th>
                                    <th scope="col">Birthdate</th>
                                    <th scope="col">Active</th>
                                    <th scope="col">State User Id</th>
                                    <th scope="col">Created At</th>
                                    <th scope="col">Update At</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $item)
                                    <tr style="text-align:center;font-weight: 100;color: #5c5f60 ;font-size: 12px;">
                                        <th scope="col">{{$item['id']}}</th>
                                        <th style="font-weight: 100">{{$item['segmentation_id']}}</th>
                                        <th style="font-weight: 100">{{$item['program_id']}}</th>
                                        <th style="font-weight: 100">{{$item['user_id']}}</th>
                                        <th style="font-weight: 100">{{$item['identification_type_id']}}</th>
                                        <th style="font-weight: 100">{{$item['identification_number']}}</th>
                                        <th style="font-weight: 100">{{$item['mobile_number']}}</th>
                                        <th style="font-weight: 100">{{$item['meta']}}</th>
                                        <th style="font-weight: 100">{{$item['birth_date']}}</th>
                                        <th style="font-weight: 100">{{$item['active']}}</th>
                                        <th style="font-weight: 100">{{$item['state_user_id']}}</th>
                                        <th style="font-weight: 100">{{$item['created_at']}}</th>
                                        <th style="font-weight: 100">{{$item['updated_at']}}</th>
                                        <th style="font-weight: 100"><a href="{{ url('users/'. $item['user_id'] . '/'. 'transaction') }}"type="button" class="btn btn-primary btn-sm" title="Ver transacciones">Consultar</a></th>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="d-flex">
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>