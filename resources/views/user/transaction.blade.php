<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Ready | Test</title>
</head>

<body>
    <div class="container" style="margin-top:50px">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        Transacciones Registradas
                    </div>
                    <div class="card-body table-responsive">
                        <table class="table table-striped table-hover">
                            <thead class="table-dark">
                                <tr style="text-align:center;font-size: 12px;">
                                    <th scope="col">id</th>
                                    <th scope="col">Cliente</th>
                                    <th scope="col">Segmentation</th>
                                    <th scope="col">Transaction Type</th>
                                    <th scope="col">Transaction Status</th>
                                    <th scope="col">Transaction Currency</th>
                                    <th scope="col">Transaction Source</th>
                                    <th scope="col">Year</th>
                                    <th scope="col">Month</th>
                                    <th scope="col">Amount</th>
                                    <th scope="col">Transaction Detail</th>
                                    <th scope="col">Created At</th>
                                    <th scope="col">Update At</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($transaction as $item)
                                    <tr style="text-align:center;font-weight: 100;color: #5c5f60 ;font-size: 12px;">
                                        <th scope="col">{{$item['id']}}</th>
                                        <th style="font-weight: 100">{{$item['client_id']}}</th>
                                        <th style="font-weight: 100">{{$item['segmentation_id']}}</th>
                                        <th style="font-weight: 100">{{$item['transaction_type_id']}}</th>
                                        <th style="font-weight: 100">{{$item['transaction_status_id']}}</th>
                                        <th style="font-weight: 100">{{$item['transaction_currency_id']}}</th>
                                        <th style="font-weight: 100">{{$item['transaction_source_id']}}</th>
                                        <th style="font-weight: 100">{{$item['year']}}</th>
                                        <th style="font-weight: 100">{{$item['month']}}</th>
                                        <th style="font-weight: 100">{{$item['amount']}}</th>
                                        <th style="font-weight: 100">{{$item['transaction_detail']}}</th>
                                        <th style="font-weight: 100">{{$item['created_at']}}</th>
                                        <th style="font-weight: 100">{{$item['updated_at']}}</th>
                                    </tr>
                                    @empty  
                                    <p class="bg-secondary text-white p-1">El usuario no cuenta con transacciones</p>      
                                    @endforelse
                            </tbody>
                        </table>
                        <div class="d-flex">
                            {{ $transaction->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
    -->
</body>

</html>