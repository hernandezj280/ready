<?php

namespace App\Http\Controllers;

use App\Exceptions\AppException;
use App\Models\Animal;
use App\Models\AnimalFood;
use App\Models\ApiError;
use App\Models\brand;
use App\Models\Breed;
use App\Models\EmployeeType;
use App\Models\Pharmacy;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\Specie;
use App\Models\User;
use App\Models\SystemLog;
use App\Models\UnitMeasurement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Utils\ResponseUtil;
use Laracasts\Flash\Flash;
use Response;
use Config;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;

class AppBaseController extends Controller
{

    public function sendResponse($result, $message){
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404){
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function sendSuccess($message){
        return Response::json([
            'success' => true,
            'message' => $message
        ], 200);
    }

    protected function jsonResponse($resource) {

        $response = [
            'message'   => $resource->message,
            'code'      => $resource->code,
            'isError'   => $resource->isError,
            'errors'    => $resource->errors,
            'data'      => ''
        ];

        if(isset($resource->resource)) {
            $response['data'] = $resource->resource;
        } else {
            $response['data'] = $resource->modelo;
        }

        return response()->json($response, ($resource->code != 204)  ? $resource->code : 200);
    }

    public function setLog($sMessage, $iType = SystemLog::Info) {

        $values = request()->route()->getAction();

        $controller = $values["controller"];
        $method = request()->route()->getActionMethod();

        try {

            $userId = "";

            if (Auth::check()) {
                $userId = auth()->user()->id;
            }

            /***
             *  SystemLog::create([
                'controller' => $method."->".$controller,
                'message' => $sMessage,
                'type' => $iType,
                'userId' => $userId,
            ]);

            */

            if ($iType ==  SystemLog::Exception)
            Log::error($controller . "-" . $userId . "-" . $sMessage);

            else if ($iType == SystemLog::Info)
            Log::info($controller . "-" . $userId . "-" . $sMessage);

            else if ($iType == SystemLog::Warning)
            Log::warning($controller . "-" . $userId . "-" . $sMessage);

        } catch (\Exception $e) {
            $sMessage = $e->getMessage();
            #dd($sMessage);
        }
    }

    public function updateStatus(Request $request){
        $apiError =  new ApiError();
        # SELECCIONAMOS LA TABLA A LA CUAL SE LE VA A ACTUALIZAR
        if($request->table == 'users') $row = User::whereId($request->id)->first();
        if($request->table == 'animals') $row = Animal::whereId($request->id)->first();
        if($request->table == 'breeds') $row = Breed::whereId($request->id)->first();
        if($request->table == 'species') $row = Specie::whereId($request->id)->first();
        if($request->table == 'employee_types') $row = EmployeeType::whereId($request->id)->first();
        if($request->table == 'pharmacies') $row = Pharmacy::whereId($request->id)->first();
        if($request->table == 'products_types') $row = ProductType::whereId($request->id)->first();
        if($request->table == 'brands') $row = brand::whereId($request->id)->first();
        if($request->table == 'animals_food') $row = AnimalFood::whereId($request->id)->first();
        if($request->table == 'products') $row = Product::whereId($request->id)->first();
        if($request->table == 'unit_measurement') $row = UnitMeasurement::whereId($request->id)->first();
        if($request->table == 'clients') $row = Client::whereId($request->id)->first();


        try {
            if (empty($row)) throw new AppException("Registro no encontrado.");

            $row->enabled = !$row->enabled;
            $row->save();
            $apiError->modelo = $row;
            #$description = "updateStatus" . " " . $row->name;
            #$this->saveHistory('patch',$request->table, $description);
        } catch (AppException $ex ) {
            $apiError->setError($ex->getMessage());
        } catch (\Exception $ex ) {
            #$this->setLog("Linea: ".$ex->getLine()." | MSG: ".$ex->getMessage(), SystemLog::Exception);
            $apiError->setError($ex->getMessage());
        }

        return response()->json($apiError);
    }

    public function saveHistory($method,$service,$description){
        try{

            $apiError =  new ApiError();
            $user = Auth::user();
            /***
               $row = CallHistory::create([
                'userId'        => $user->id,
                'platformId'    => $platform->id,
                'method'        => $method,
                'service'       => $service,
                'description'   => $description
            ]);

            */

            $apiError->modelo = $row;
        }catch (\AppException $ex ) {
            $apiError->setError($ex->getMessage());
        } catch (\Exception $ex ) {
            $this->setLog("Linea: ".$ex->getLine()." | MSG: ".$ex->getMessage(), SystemLog::Exception);
            $apiError->setError($ex->getMessage());
        }
        return response()->json($apiError);
    }

    public function saveFindings($file,$emrId){
        try{

            $apiError = new ApiError();

            $base64Image = explode(";base64,", $file);
            $explodeImage = explode("image/", $base64Image[0]);
            $imageType = $explodeImage[1];
            $imageBase64 = base64_decode($base64Image[1]);

            $directory = public_path() . '/findings/' . date('Y') . '/' . $emrId . '/' ;
            $fileName = uniqid() . "_" . time() . '.'. $imageType;
            $sPath = $directory . $fileName;
            $rPath = '/findings/' . date('Y') . '/' . $emrId . '/' . $fileName;

            if (!File::exists($directory)) {
                File::makeDirectory($directory , 0755, true);
            }

            file_put_contents($sPath, $imageBase64);
            //File::move($file, $sPath);
            $apiError->data = $rPath;
            return $apiError;
        }catch (\AppException $ex ) {
            $apiError->setError($ex->getMessage());
        }catch (\Exception $ex ) {
            $this->setLog("Linea: ".$ex->getLine()." | MSG: ".$ex->getMessage(), SystemLog::Exception);
            $apiError->setError($ex->getMessage());
        }
    }

    public function storageLogin(){
        $apiError = new ApiError();
        try {

            $client = new Client(['verify' => false]);
            $response = $client->request('POST', config('storefile.baseurl').'/api/auth/login', [
                'form_params'  => [
                    'email'    => config('storefile.user'),
                    'password' => config('storefile.password')
                ]
            ]);
            $apiError->data = json_decode($response->getBody()->getContents(), true);
        } catch (AppException $ex){
            #$this->setLog("StorageService", "getToken", $ex->getMessage(), $ex);
            $apiError->setError($ex->getMessage());
        } catch (\Exception $ex) {
            #$this->setLog("StorageService", "getToken", $ex->getMessage(), $ex);
            $apiError->setError($ex->getMessage());
        }
        return $apiError;
    }
}
