<?php

namespace App\Http\Controllers;

use App\Exceptions\AppException;
use App\Models\ApiError;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use InfyOm\Generator\Utils\ResponseUtil;
use Laracasts\Flash\Flash;
use Response;
use Config;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\File;

class AppBaseController extends Controller
{

    public function sendResponse($result, $message){
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404){
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function sendSuccess($message){
        return Response::json([
            'success' => true,
            'message' => $message
        ], 200);
    }

    protected function jsonResponse($resource) {

        $response = [
            'message'   => $resource->message,
            'code'      => $resource->code,
            'isError'   => $resource->isError,
            'errors'    => $resource->errors,
            'data'      => ''
        ];

        if(isset($resource->resource)) {
            $response['data'] = $resource->resource;
        } else {
            $response['data'] = $resource->modelo;
        }

        return response()->json($response, ($resource->code != 204)  ? $resource->code : 200);
    }

    public function setLog($sMessage, $iType) {

        try {

            $values = request()->route()->getAction();

            $controller = $values["controller"];
            $method = request()->route()->getActionMethod();
            
            if ($iType == "error")
            Log::error($controller . "-" . $sMessage);

            else if ($iType == "info")
            Log::info($controller . "-" . "-" . $sMessage);

            else if ($iType == "warning")
            Log::warning($controller . "-" . $sMessage);

        } catch (\Exception $e) {
            $sMessage = $e->getMessage();
        }
    }
}
