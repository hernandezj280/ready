<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;


class UserController extends Controller
{

    private $service;

    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    public function index() {
        $result = $this->service->findUsers();
        return view('user.index')->with('users', $result->modelo);
    }

    public function transaction($id) {
        $result = $this->service->findTransaction($id);
        return view('user.transaction')->with('transaction', $result->modelo);
    }

    public function getUser($id) {
        $apiError = $this->service->findUserByUserId($id);
        return response()->json($apiError);
    }

}
