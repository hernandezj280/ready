<?php

namespace App\Interfaces;
use Illuminate\Http\Request;

interface  IServices
{
    public function findUsers();
    public function findTransaction($clientId);
    public function findUserByUserId($id);


}