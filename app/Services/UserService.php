<?php

namespace App\Services;

use App\Http\Controllers\AppBaseController;
use App\Interfaces\IServices;
use App\Models\ApiError;
use App\Exceptions\AppException;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;


class UserService extends AppBaseController implements IServices
{

    /*Metodo que retorna un listado de usarios usuarios.*/
    public function findUsers()
    {

        try {

            $apiError =  new ApiError();
            $response = $this->getUsers();
            $users = collect($response)->sortBy('created_at')->reverse();
            $users = $this->paginate($users,10);
            $users->setPath('');
            $apiError->modelo = $users;
            return $apiError;

        } catch (AppException $ex) {
            $this->setLog("findUsers","error");
            $apiError->setError($ex->getMessage());
        } catch (\Exception $ex) {
            $this->setLog("findUsers","error");
            $apiError->setError($ex->getMessage());
        }
        return $apiError;
    }

    /*Metodo que retorna un listado de transacciones de un usuarios.*/
    public function findTransaction($clientId)
    {
        try {

            $apiError =  new ApiError();
            $response = $this->getTransaction($clientId);
            $transaction = collect($response)->sortBy('created_at')->reverse();
            $transaction = $this->paginate($transaction,10);
            $transaction->setPath('');
            $apiError->modelo = $transaction;
            return $apiError;
        } catch (AppException $ex) {
            $this->setLog("findTransaction","error");
            $apiError->setError($ex->getMessage());
        } catch (\Exception $ex) {
            $this->setLog("findTransaction","error");
            $apiError->setError("Error, consulte con su administrador.");
        }
        return $apiError;
    }

    /*Metodo que retorna la información de un usario y sus transacciones.*/
    public function findUserByUserId($id)
    {

        try {

            $apiError =  new ApiError();

            $users = $this->getUsers();
            $users = collect($users);
            $user  = $users->where('user_id', '=', $id)->first();
            
            $transaction = $this->getTransaction($user['user_id']);
            $transaction = collect($transaction)->sortBy('created_at')->reverse();

            $user['transaction'] = $transaction;
            $apiError->modelo = $user;
            $this->setLog("API: findUserByUserId","info");
            return $apiError;
        } catch (AppException $ex) {
            $this->setLog("API: findUserByUserId","error");
            $apiError->setError($ex->getMessage());
        } catch (\Exception $ex) {
            $this->setLog("API: findUserByUserId","error");
            $apiError->setError($ex->getMessage());
        }
        return $apiError;
    }

    /* Metodo que consume API para obtener un listado de usuarios.*/
    public function getUsers(){

        try {

            $apiError =  new ApiError();
            $url = env("API_URL_READY") . "/users/" . env("API_TOKEN_READY");
            $response = Http::get($url);
            $users = $response->json();
            return $users;
        } catch (AppException $ex) {
            $this->setLog("API: getUsers","error");
            $apiError->setError($ex->getMessage());
        } catch (\Exception $ex) {
            $this->setLog("API: getUsers","error");
            $apiError->setError($ex->getMessage());
        }
        return $apiError;


    }

    /* Metodo que consume API para obtener un listado de transacciones por user_id.*/
    public function getTransaction($clientId)
    {
        try {

            $apiError =  new ApiError();
            $url = env("API_URL_READY") . "/users/" . env("API_TOKEN_READY") . '/transaction/' . $clientId;
            $response = Http::get($url);
            $transaction = $response->json();
            return $transaction;
        } catch (AppException $ex) {
            $this->setLog("API: getTransaction","error");
            $apiError->setError($ex->getMessage());
        } catch (\Exception $ex) {
            $this->setLog("API: getTransaction","error");
            $apiError->setError("Error, consulte con su administrador.");
        }
        return $apiError;
    }

    /*Metodo generico o customizado para paginar un array.*/
    public function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
